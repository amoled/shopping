<?php

use Illuminate\Support\Facades\Route;


Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');

Route::middleware('auth:api')->group(function () {
    Route::resource('products', 'ProductController');
    Route::resource('product-groups', 'ProductGroupController');
    Route::resource('orders', 'OrderController');
});


