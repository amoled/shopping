<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductGroup;
use Faker\Generator as Faker;

$factory->define(ProductGroup::class, function (Faker $faker) {
    return [
        'title' => $faker->company,
        'description' => $faker->text
    ];
});
