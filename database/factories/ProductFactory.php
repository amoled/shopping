<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\ProductGroup;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'product_group_id' => factory(ProductGroup::class),
        'title' => $faker->company,
        'description' => $faker->text,
        'image' => 'image.jpg',
        'price' => $faker->numberBetween(10000, 10000000),
        'available' => $faker->numberBetween(10,1000),
        'created_at' => now()
    ];
});
