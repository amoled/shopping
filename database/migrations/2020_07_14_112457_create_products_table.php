<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_group_id')->unsigned()->nullable();
            $table->string('title');
            $table->text('description');
            $table->string('image')->nullable();
            $table->decimal('price',12,2);
            $table->unsignedBigInteger('available');
            $table->timestamps();

            $table->foreign('product_group_id')
                ->references('id')
                ->on('product_groups')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
