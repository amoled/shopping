<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    protected $fillable = ['title', 'description'];

    //Get the resource path
    public function path()
    {
        return '/product-groups/' . $this->id;
    }

    public function scopeTitle($query, $title)
    {
        if($title) {
            return $query->where('title', 'LIKE', '%' . $title . '%');
        }
    }

    public function scopeDescription($query, $description)
    {
        if($description) {
            return $query->where('description', 'LIKE', '%' . $description . '%');
        }
    }

    //Define One to Many relationship between ProductGroup and Product Models
    public function product()
    {
        return $this->hasMany(ProductGroup::class, 'product_group_id', 'id');
    }
}
