<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['product_group_id', 'title', 'description', 'photo', 'price', 'available'];

    //Get the resource path
    public function path()
    {
        return '/products/' . $this->id;
    }

    protected $attributes = [
        'image' => 'image.jpg',
    ];

    public function scopeTitle($query, $title)
    {
        if($title) {
            return $query->where('title', 'LIKE', '%' . $title . '%');
        }
    }

    public function scopeDescription($query, $description)
    {
        if($description) {
            return $query->where('description', 'LIKE', '%' . $description . '%');
        }
    }

    //Define One to Many inverse relationship between Product and ProductGroup Models
    public function group()
    {
        return $this->belongsTo(ProductGroup::class, 'product_group_id');
    }
}
