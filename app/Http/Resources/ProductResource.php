<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'product_id' => $this->id,
                'group_title' => $this->group ? $this->group->title : '-',
                'title' => $this->title,
                'description' => $this->description,
                'image' => $this->image,
                'price' => number_format($this->price),
                'available' => $this->available,
                'last_updated' => $this->updated_at->diffForHumans(),
            ],
            'links' => [
                'self' => $this->path(),
            ]
        ];
    }
}
