<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'order_id' => $this->id,
                'placed_by' => $this->user->name,
                'last_updated' => $this->updated_at->diffForHumans(),
                'total_price' => number_format($this->product->sum('pivot.price')),
                'products' => ProductResource::collection($this->product),
            ],
            'links' => [
                'self' => $this->path(),
            ]
        ];
    }
}
