<?php

namespace App\Http\Controllers;

use App\ProductGroup;
use App\Http\Resources\ProductGroupResource;
use Symfony\Component\HttpFoundation\Response;

class ProductGroupController extends Controller
{
    public function index()
    {
        $productGroups = ProductGroup::Title(request('title'))
            ->Description(request('description'))
            ->orderBy('id', 'DESC')
            ->get();

        return ProductGroupResource::collection($productGroups);
    }

    public function store()
    {
        $productGroups = ProductGroup::create($this->validatedData());

        return (new ProductGroupResource($productGroups))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(ProductGroup $productGroup)
    {
        return new ProductGroupResource($productGroup);
    }

    public function update(ProductGroup $productGroup)
    {
        $data = $this->validatedData();

        $productGroup->update($data);

        return (new ProductGroupResource($productGroup))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(ProductGroup $productGroup)
    {
        $productGroup->delete();

        return response(['data' => 'Successfully deleted the product group'], Response::HTTP_NO_CONTENT);
    }

    /**
     * @return array
     */
    private function validatedData(): array
    {
        return request()->validate([
            'title' => 'required',
        ]);
    }
}
