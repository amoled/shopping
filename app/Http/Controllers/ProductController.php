<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductGroup;
use App\Http\Resources\ProductResource;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::Title(request('title'))
            ->Description(request('description'))
            ->orderBy('id', 'DESC')
            ->get();

        return ProductResource::collection($products);
    }

    public function store()
    {
        $product = Product::create($this->validatedData());

        return (new ProductResource($product))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    public function update(Product $product)
    {
        $data = $this->validatedData();

        $product->update($data);

        return (new ProductResource($product))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return response(['data' => 'Successfully deleted the product'], Response::HTTP_NO_CONTENT);
    }

    /**
     * @return array
     */
    private function validatedData(): array
    {
        return request()->validate([
            'product_group_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'available' => 'required',
        ]);
    }
}
