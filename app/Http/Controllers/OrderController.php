<?php

namespace App\Http\Controllers;

use App\Order;
use App\Http\Resources\OrderResource;
use App\Product;
use http\Client\Curl\User;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function index()
    {

        $products = Order::UserID(request('user_id'))
            ->orderBy('id', 'DESC')
            ->get();

        return OrderResource::collection($products);
    }

    public function store()
    {
        $data = $this->validatedData();

        $products = $this->getOrderProducts($data['products']);

        if(count($products) > 0) {
            $order = auth()->user()->order()->save(new Order());
            $order->product()->sync($products);

            return (new OrderResource($order))
                ->response()
                ->setStatusCode(Response::HTTP_CREATED);
        }

        return response('Selected products are unavailable!')
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function show(Order $order)
    {
        return new OrderResource($order);
    }

    public function update(Order $order)
    {
        $this->authorize('update', $order);

        $data = $this->validatedData();

        $order->update($data);

        return (new OrderResource($order))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(Order $order)
    {
        $this->authorize('delete', $order);

        $order->delete();

        return response(['data' => 'Successfully deleted the product'], Response::HTTP_NO_CONTENT);
    }

    /**
     * @return array
     */
    private function validatedData(): array
    {
        return request()->validate([
            'products' => 'required',
            'products.*' => 'required|numeric',
        ]);
    }

    /**
     * Check for availability of products and returns a list of products and prices
     * @param array $requestedProducts
     * @return array of requested products and prices
     */
    private function getOrderProducts($requestedProducts): array
    {
        $products = [];

        foreach ($requestedProducts as $productId) {
            $product = Product::find($productId);
            if (($product instanceof Product) && ($product->available > 0)) {
                $products[$productId] = ['price' => $product->price];
                $product->update(['available' => --$product->available]);
            }
        }
        return $products;
    }
}
