<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //Get the resource path
    public function path()
    {
        return '/orders/' . $this->id;
    }

    public function scopeUserId($query, $userId)
    {
        if($userId)
            return $query->where('user_id', $userId);
    }


    //Define One to Many inverse relationship between Order and User Models
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    //Define Many to Many relationship between Order and Product Models
    public function product()
    {
        return $this->belongsToMany(Product::class, 'order_products')
            ->withPivot('price')
            ->withTimestamps();
    }

}
